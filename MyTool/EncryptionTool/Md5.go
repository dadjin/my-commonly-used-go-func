package EncryptionTool

import (
	"crypto/md5"
	"encoding/hex"
)

func Md5String(getString string) string {
	data := []byte(getString)
	md5byte := md5.Sum(data)
	md5String := hex.EncodeToString(md5byte[:])
	return md5String
}
