package EncryptionTool

// xorBytes 快速异或操作，使用 copy 函数
func XorBytes(data, key []byte, datalen int) []byte {
	if datalen < 0 {
		datalen = len(data)
	}
	keylen := len(key)
	result := make([]byte, datalen)
	copy(result, data) // 先复制 a 的内容到结果

	for i := 0; i < datalen; i++ {
		if i >= keylen {
			break
		}
		result[i] ^= key[i] // 直接在结果上进行异或
	}

	return result
}

// reverseAndSwapPairs 将传入的字节切片倒序，并交换每一对相邻元素
func reverse(data []byte, length int) []byte {
	// 倒序
	if length < 0 {
		length = len(data)
	}
	for i := 0; i < length/2; i++ {
		data[i], data[length-i-1] = data[length-i-1], data[i]
	}

	// 交换每一对相邻元素,如果i+1不存在就跳过
	return data
}

func swapPairs(data []byte, length int) []byte {
	if length < 0 {
		length = len(data)
	}

	for i := 0; i+1 < length; i += 2 {
		//判断data[i+1]是否存在
		data[i], data[i+1] = data[i+1], data[i]
	}
	return data
}

// 异或密文
func XorBytesShuffleEncrypt(data, key []byte) []byte {
	datalen := len(data)
	return XorBytes(swapPairs(reverse(XorBytes(data, key, datalen), datalen), datalen), key, datalen)
}

// 异或解密文
func XorBytesShuffleDecrypt(data, key []byte) []byte {
	datalen := len(data)
	return XorBytes(reverse(swapPairs(XorBytes(data, key, datalen), datalen), datalen), key, datalen)
}

/*
//DEMO
func main() {
	a := []byte{0x1f, 0x2a, 0x3c, 0x4d}
	b := []byte("11222222asd")

	am := XorBytesShuffleDecrypt(a, b)
	fmt.Println(am)
	newa := XorBytesShuffleEncrypt(am, b)
	fmt.Println(newa)

}
*/
