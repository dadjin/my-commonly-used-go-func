package EncryptionTool

import (
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
	"fmt"
)

func Sha1String(getString string) string {
	h := sha1.New()
	h.Write([]byte(getString))
	bs := h.Sum(nil)
	return hex.EncodeToString(bs)
}

func Sha256String(getString string) string {
	// 将字符串转换为字节切片
	hasher := sha256.New()
	hasher.Write([]byte(getString)) // 写入数据
	sha256Hash := hasher.Sum(nil)   // 计算哈希值
	// 将字节切片转换为十六进制字符串
	return fmt.Sprintf("%x", sha256Hash)
}

func Sha512String(getString string) string {
	// 将字符串转换为字节切片
	hasher := sha512.New()
	hasher.Write([]byte(getString)) // 需要将字符串转换为字节切片
	hashBytes := hasher.Sum(nil)    // 计算哈希值
	// 将字节切片转换为十六进制字符串
	return fmt.Sprintf("%x", hashBytes)
}
