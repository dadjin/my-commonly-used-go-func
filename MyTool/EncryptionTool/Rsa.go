package EncryptionTool

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
)

/*与DadjinTool的PHP工具的RSA互通*/
// 解析公钥字符串
func ParseRsaPublicKey(pubKeyStr string) (*rsa.PublicKey, error) {
	block, _ := pem.Decode([]byte(pubKeyStr))
	if block == nil {
		return nil, errors.New("failed to decode public key")
	}

	pubInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		cert, err := x509.ParseCertificate(block.Bytes)
		if err != nil {
			return nil, fmt.Errorf("failed to parse public key: %v", err)
		}
		pubInterface = cert.PublicKey
	}

	pubKey, ok := pubInterface.(*rsa.PublicKey)
	if !ok {
		return nil, errors.New("public key is not an RSA public key")
	}
	return pubKey, nil
}

// 解析私钥字符串
func ParseRsaPrivateKey(privKeyStr string) (*rsa.PrivateKey, error) {
	block, _ := pem.Decode([]byte(privKeyStr))
	if block == nil {
		return nil, errors.New("failed to decode private key")
	}

	privInterface, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err == nil {
		return privInterface, nil
	}

	privAny, err := x509.ParsePKCS8PrivateKey(block.Bytes)
	if err != nil {
		return nil, fmt.Errorf("failed to parse private key: %v", err)
	}

	privKey, ok := privAny.(*rsa.PrivateKey)
	if !ok {
		return nil, errors.New("private key is not an RSA private key")
	}
	return privKey, nil
}

// 使用公钥加密数据
func RsaEncrypt(publicKey *rsa.PublicKey, message []byte) ([]byte, error) {
	return rsa.EncryptPKCS1v15(rand.Reader, publicKey, message)
}

// 使用私钥解密数据
func RsaDecrypt(privateKey *rsa.PrivateKey, encryptedData []byte) ([]byte, error) {
	return rsa.DecryptPKCS1v15(rand.Reader, privateKey, encryptedData)
}

/*示例*/
/*
func main() {
	// 示例公钥和私钥字符串
	pubKeyStr := `-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAyxrVbnwMFYcUeB92/30W
6sDdtcoND9958ULpDKc9+58SFD3I1fEu5Phpq/kMyIZB/69oRb2jS4/xBrQOkZ/Q
geH8+ABy/YwM6W3KUlYOCnnWOfhv6C5oi2vtRKK0LhooQjq1mI+O9YYt6V7c5tMu
NcXcG+kEJHryprhpMNlkApCPz6W2BVhVtOIuzgHLzbTRQb2JT/PyhKSLCQQ0SxWr
goHhHhqNRYQMeA3Je33P84gMcucOq58pUiJD14LEQnPCgNLE1BIepcZ+MP98pKPC
e/ScOnocy9NaNdjJpM54Ptjb5iwGNqCdPTPUM4i34FhTzpJcVPLGhJxZXyFU3/CX
myA/sHtOXshacV+9+iGHl+EIe0BUY3Z+9BK+IAlnZ7VpjlghTH6ed4USbbZKPzJd
wadlFbRp3KUOcqVF2cWmanEtnTtri7m0KnxmWjjhBEz044zahuDvKHMG04mhw9Vl
QivwybJGl12Qbaayjm6gEtUIt46+ei3nWhCS0RscQHl7JT4f0gmNIDg8z5Pr72sp
pk7wD2NR+dbMrmYeqd7uTpnfAlseWNqhfNWJfLElN67HDvv1Q4jiuY+sZWbqyPzN
bAHXsAawcqBgNABAd9tJC14jb7oWpe6RTBbolupnYE+mYZvlN7I8GkC4YlMLC7Xg
awWIThSJKDfXTbFhs1s7CFECAwEAAQ==
-----END PUBLIC KEY-----
`
	privKeyStr := `-----BEGIN PRIVATE KEY-----
MIIJQwIBADANBgkqhkiG9w0BAQEFAASCCS0wggkpAgEAAoICAQDLGtVufAwVhxR4
H3b/fRbqwN21yg0P33nxQukMpz37nxIUPcjV8S7k+Gmr+QzIhkH/r2hFvaNLj/EG
tA6Rn9CB4fz4AHL9jAzpbcpSVg4KedY5+G/oLmiLa+1EorQuGihCOrWYj471hi3p
Xtzm0y41xdwb6QQkevKmuGkw2WQCkI/PpbYFWFW04i7OAcvNtNFBvYlP8/KEpIsJ
BDRLFauCgeEeGo1FhAx4Dcl7fc/ziAxy5w6rnylSIkPXgsRCc8KA0sTUEh6lxn4w
/3yko8J79Jw6ehzL01o12Mmkzng+2NvmLAY2oJ09M9QziLfgWFPOklxU8saEnFlf
IVTf8JebID+we05eyFpxX736IYeX4Qh7QFRjdn70Er4gCWdntWmOWCFMfp53hRJt
tko/Ml3Bp2UVtGncpQ5ypUXZxaZqcS2dO2uLubQqfGZaOOEETPTjjNqG4O8ocwbT
iaHD1WVCK/DJskaXXZBtprKObqAS1Qi3jr56LedaEJLRGxxAeXslPh/SCY0gODzP
k+vvaymmTvAPY1H51syuZh6p3u5Omd8CWx5Y2qF81Yl8sSU3rscO+/VDiOK5j6xl
ZurI/M1sAdewBrByoGA0AEB320kLXiNvuhal7pFMFuiW6mdgT6Zhm+U3sjwaQLhi
UwsLteBrBYhOFIkoN9dNsWGzWzsIUQIDAQABAoICAQCDgbJkQUmJvtbQGH9yWco2
4f4sMNFYT7ijMFG5cag+ffa0yHR7ohhdBUNWqIFQbS3aofaAAZPz3xWO45LzU2ks
T+j9ozzRUpchpYb03OdmfRDqpCPBUBs8+vq8ECVeHG2oxF7Ek19bNjAQBO4/PsmS
47PrFmsqO7lVnd3uVTPePUAAkSlUOKf14OWxWZRecw9y44g/DpMes3OauPArFbLh
Y7shRMvoEOIJ8dNddE7rPlHhLB5hnUKh/bGznuX4qP9tbgsnzTuu1uDqVOKdr7LD
k9R8QIjYqNamOuIFJfLRrpRjamnzgPeOozpoy8NTLZ3GCRpjnnZPcvSjFe/gt+VQ
0rPMjK94V/nKO0nP2LUEvQ4vVX68KIUfb4YfGhJRp7VZ5gY6MkcRaxSzr0V+I2SB
waPTKSdELNNlfE8OO/5MFLFf5RG717Rk2RFPCc8+B0WuaGxSHW733PohiVIF8hmU
PJO7vQO/ed3bTx+Auua2bqDVtrFw7lOUslzMpuR2w3ep7A8sHzLyj3Am8SKAQe2T
KbddegIhd42WJFBQtBePJI4Wk7HmxMAXq+7NqQUA0VYOMXa6nWg2PO75fmorMktK
tBKIZqgXbxrE+a8AsU3PshsqTkNGaSUvT8H47PNjqQSXQtJ3RvcczJkH7GFg99Sy
ibQH+hE6O4K8nHDWZArwAQKCAQEA56dUVtziah8o4BRlwUyN1VrdqzKcwU077bMB
aXeC3/MDY2XBdVAeJRukShGsyQEVvt/ZdkvZTY7AL4l5NRSJoHvWukwXzGBrX7UH
GWqa9iq9F68IA09zyHg3eTRqojtwMD0QoXmB+zRHysr/Y6Qcb3xXdUf2+TfplMYi
merBdNU8BLj3S2LKgujra5jvN87NG8lRROWej3zhpaAYxWDaoxLltxDfOE0+qzza
PeeazM5K3oTckQiOgQQtHJBJA1uJiqBav6wwhkjToCWaGFaldLeND/EIVFjkXyvK
GH41iZCPU9xYaCkZyhIGuzHFhdMOZeZ1JuPZi/HriaEYY5JS0QKCAQEA4HNlFIdF
OxIvk4jWlwEN6xU44iKr0U/V96qpcMmWLT9qoq815aTc2t2QjoqDqW1voG4KP4a/
iyScS/wkQ/9BEfYsQKhVc8ZeRnapACBAJ8VfxQRWvII+y7nZr//9DXkBi67xZXU0
k53WzElXQqKSLqEXHcaI/7ByXb8mWBD+NEHUNRBE62rvwABq8uZ15l0x1uSggEd8
lQJTwbjsRDpJvsGer/JWG8QgXNDj5Wc6jnWHfL3BbLRYFHQUvHlOZKuvk3RoRhvV
kBf0hDrmD5kLeqoO10mgwtkqxIgUmxsm6Qa5tPdgT/XBeD5Y9E0S1BCIa4RAfQpb
RpkK2iYDamu9gQKCAQEA2wnNJZUWQWHVNIwU6wGH2wVgNFBXCDAPSmLIIwFuLnOJ
8KJ2moZ2+n94iNDNoAH2ndTrun9dqYghENHcWeRhfe2ay+zHU+iXnpd9bvruK3Wd
9pb57Kdjw1pomaKGcX/iRLp4zzYlQXHdsFLhyUWT3Zak27CARoYnxxTSAteGtQIr
DgwXSKDQ+5dhgwvIpexU5MEmpw63LtXWRtVqlrdOBIQJ0V+SsZnYd/HCKmakaKNn
B6B5Lmn41OUuAqQrZZFYLEwTgsk92Ltj3zmuyvSi3gnwx/IQ/jBPP+4X2bD8zmZy
H8lpjpPOXtNMqhQVwMQgCRowC+XF/h1xqWE5vmzNAQKCAQBcnQDeYZqdbzmWZOY2
1MaLc+xO0wOWkJy3XwIfee6vqPU1P5q7sTU4UigtyNXe+TsQNZeg8tbicS7U46uL
Ky4MXUmRPGRWK0oHRTsixCv0vSpxPwMTMaL7qSo7xyMBhiavHnaY6K3TI1PYY4LQ
CgjLmMjTDnLFHmdya0dFG23o3ijT5GyrvypCTWnT5GhNr7K6+weHJvgu/BDXZJiI
5ow7W+VAsHv7ZQf9hGwk4l11HuXTAPFpC1k0x1kvE9bXD0iZTXUhSKbsmBXa4frA
AOSNrtIE32qoO7e79HKcNa3uhUnndBOXqs4z8XX/CDZ2roO76Yw6jCVxkgC6Vukd
5msBAoIBAC98AOsU1SJcvJreOXMmhssKIfMVj3E9czh8jD5LAh3lqrC2Vj1ySd5t
pW9c6KSS9Tspj44A/bMd21Ty3zLbGBd4JC07TE08lmM23tfzu8HU0kgrl84lQjEn
bDPKu8hQ4ecP48OcAMTa5KOh8nxTsGLRaSjqAbBtWJerTMpNTGxZLp0x+RDHizJI
pQ0ETTiauf3iNwgyOvXgJpMCWtT3GJAJtP58xBeH6cUVum++E+v9W4X4DkOYug9Z
sRhLD35s+Xce3xVauUIxKZZmlZu1zyngCdIsXdE5zykvw/DM0HvQEhOh4SZDQkw4
LDk9XTjDSpy9CVbAmgGr16mb4JUOwXU=
-----END PRIVATE KEY-----
`

	// 解析公钥和私钥
	publicKey, err := ParseRsaPublicKey(pubKeyStr)
	if err != nil {
		fmt.Println("Failed to parse public key:", err)
		return
	}

	privateKey, err := ParseRsaPrivateKey(privKeyStr)
	if err != nil {
		fmt.Println("Failed to parse private key:", err)
		return
	}

	stringmsg := "Hello, RSA encryption and decryption!"
	message := []byte(stringmsg)

	// 加密消息
	encryptedData, err := RsaEncrypt(publicKey, message)
	if err != nil {
		fmt.Println("Encryption failed:", err)
		return
	}
	fmt.Println("Encrypted data:", base64.StdEncoding.EncodeToString(encryptedData))

	// 解密消息
	decryptedData, err := RsaDecrypt(privateKey, encryptedData)
	if err != nil {
		fmt.Println("Decryption failed:", err)
		return
	}
	fmt.Println("Decrypted data:", string(decryptedData))
}

*/
