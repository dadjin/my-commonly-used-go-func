package xdb

import "strings"

type IpInfo struct {
	Country  string //国家
	Region   string //区域
	Province string
	City     string //城市
	ISP      string //运营商
	IP       string //IP
}

func (s *Searcher) SearchByCustomInfo(str string) (IpInfo, error) {
	region, err := s.SearchByStr(str)
	thisIpInfo := IpInfo{}
	if err != nil {
		return thisIpInfo, err
	}
	strArr := strings.Split(region, "|")
	thisIpInfo.Country = strArr[0]
	thisIpInfo.Region = strArr[1]
	thisIpInfo.Province = strArr[2]
	thisIpInfo.City = strArr[3]
	thisIpInfo.ISP = strArr[4]
	thisIpInfo.IP = str
	return thisIpInfo, err
}
