package VCTypeTool

// 将 []string 转换为 []interface{} 类型的切片
func StringSliceToInterfaceSlice(strSlice []string) []interface{} {
	result := make([]interface{}, len(strSlice))
	for i, v := range strSlice {
		result[i] = v
	}
	return result
}

// 将 []interface{} 转换为 []string 类型的切片
func InterfaceSliceToStringSlice(interfaceSlice []interface{}) []string {
	result := make([]string, len(interfaceSlice))
	for i, v := range interfaceSlice {
		result[i] = v.(string)
	}
	return result
}
