package VCTypeTool

import "fmt"

// 判断某一个值是否含在切片之中
func InArray(need interface{}, haystack interface{}) (bool, error) {
	switch key := need.(type) {
	case int:
		for _, item := range haystack.([]int) {
			if item == key {
				return true, nil
			}
		}
	case string:
		for _, item := range haystack.([]string) {
			if item == key {
				return true, nil
			}
		}
	case int64:
		for _, item := range haystack.([]int64) {
			if item == key {
				return true, nil
			}
		}
	case float64:
		for _, item := range haystack.([]float64) {
			if item == key {
				return true, nil
			}
		}
	default:
		return false, fmt.Errorf("InArray: need type error")
	}
	return false, nil
}
