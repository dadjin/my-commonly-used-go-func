package VCTypeTool

//翻转字符串
func ReverseString(s string) string {
	// 将字符串转换为字节数组
	bytes := []byte(s)

	// 使用两个指针交换首尾元素的位置
	left := 0
	right := len(bytes) - 1
	for left < right {
		bytes[left], bytes[right] = bytes[right], bytes[left]
		left++
		right--
	}

	// 返回翻转后的字符串
	return string(bytes)
}
