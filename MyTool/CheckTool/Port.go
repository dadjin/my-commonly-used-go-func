package CheckTool

import (
	"fmt"
	"net"
)

// 检查端口是否被使用
func CheckPortInUse(port int) bool {
	address := fmt.Sprintf(":%d", port)
	conn, err := net.Dial("tcp", address)
	if err != nil {
		return false // 端口未被占用
	}
	defer conn.Close()
	return true // 端口被占用
}
