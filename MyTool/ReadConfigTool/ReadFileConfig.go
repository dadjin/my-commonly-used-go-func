package ReadConfigTool

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"reflect"
	"strings"
)

//配置文件
func InitConfig(path string) map[string]string {
	config := make(map[string]string)

	f, err := os.Open(path)
	defer f.Close()
	if err != nil {
		panic(err)
	}

	r := bufio.NewReader(f)
	for {
		b, _, err := r.ReadLine()
		if err != nil {
			if err == io.EOF {
				break
			}
			panic(err)
		}
		s := strings.TrimSpace(string(b))
		index := strings.Index(s, "=")
		if index < 0 {
			continue
		}
		key := strings.TrimSpace(s[:index])
		if len(key) == 0 {
			continue
		}
		value := strings.TrimSpace(s[index+1:])
		if len(value) == 0 {
			continue
		}
		config[key] = value
	}
	return config
}

func JsonConfigPoint(path string) map[string]interface{} {
	openFile, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}

	defer openFile.Close()

	reader := bufio.NewReader(openFile)
	var long_bline_arr []string
	for {
		bline, isPrefix, err := reader.ReadLine()
		if err == io.EOF {
			break // 读取到文件结束才退出
		}

		// 读取到超长行，即单行超过4k字节，直接写入放入数组
		if isPrefix {
			long_bline_arr = append(long_bline_arr, string(bline))
			continue
		}
		//如果是超长字符串或短都放入数组了，所以需解析
		long_bline_arr = append(long_bline_arr, string(bline))
		//输出内容
	}
	var data interface{}
	err = json.Unmarshal([]byte(strings.Join(long_bline_arr, "")), &data)
	if err != nil {
		log.Fatal(err)
	}
	result := make(map[string]interface{})
	parseJSON(data, "", result)
	return result
}

func JsonConfig(path string) map[string]interface{} {
	openFile, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}

	defer openFile.Close()

	reader := bufio.NewReader(openFile)
	var long_bline_arr []string
	for {
		bline, isPrefix, err := reader.ReadLine()
		if err == io.EOF {
			break // 读取到文件结束才退出
		}

		// 读取到超长行，即单行超过4k字节，直接写入放入数组
		if isPrefix {
			long_bline_arr = append(long_bline_arr, string(bline))
			continue
		}
		//如果是超长字符串或短都放入数组了，所以需解析
		long_bline_arr = append(long_bline_arr, string(bline))
		//输出内容
	}

	return PaseJsonConfigToMapString(strings.Join(long_bline_arr, ""))
}

func PaseJsonConfigToMapString(jsonStr string) map[string]interface{} {
	var data interface{}
	err := json.Unmarshal([]byte(jsonStr), &data)
	if err != nil {
		log.Fatal(err)
	}
	dateMapStringInterface := data.(map[string]interface{})
	return dateMapStringInterface

	//遍历事例
	/*
	   MapMultiLayerfor(PaseJsonConfigToMapString(jsonStr))
	*/
}

//查看json解析map的结构
func MapMultiLayerfor(myMapStringinterface map[string]interface{}, tab ...int) {
	tab_num := 0
	var tab_arr []string
	if len(tab) > 0 {
		tab_num = tab[0]
	}
	for t := 0; t < tab_num; t++ {
		tab_arr = append(tab_arr, "  ")
	}

	for k, v := range myMapStringinterface {
		fmt.Print(strings.Join(tab_arr, ""))
		switch v := v.(type) {
		case string:
			fmt.Println(k, ":", v, "(string)")
		case float64:
			fmt.Println(k, ":", v, "(float64)")
		case []interface{}:
			fmt.Println(k, ":", "(array):")
			for i, u := range v {
				fmt.Println(" ", i, ":", u)
			}
		case map[string]interface{}:
			fmt.Println(k, ":", "map[string]interface:")
			MapMultiLayerfor(v, tab_num+1)
		default:
			fmt.Println(k, ":", v, "(unknown)")
			fmt.Println("type:", reflect.TypeOf(v))
		}
	}
}

//json解析 [第一层.第二层.第三层]
func parseJSON(data interface{}, prefix string, result map[string]interface{}) {
	switch val := data.(type) {
	case map[string]interface{}:
		for k, v := range val {
			newPrefix := prefix + k + "."
			parseJSON(v, newPrefix, result)
		}

	default:
		result[prefix[:len(prefix)-1]] = val
	}
}
